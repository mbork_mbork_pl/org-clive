;;; org-clive.el --- a simple weblog engine        -*- lexical-binding: t; -*-

;; Copyright (C) 2023  Marcin Borkowski

;; Author: Marcin Borkowski <mbork@mbork.pl>
;; Keywords: wp, hypermedia

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; This is an extremely simplistic weblog engine based on Org mode.
;; The design goals are:
;; - ease of use and modification
;; - 100% static file generation
;; - full description of all the weblog contents in a single Org mode file

;;; Code:

(require 'cl)
(require 'org)
(require 'ox-html)

(org-export-define-derived-backend 'clive 'html
  :translate-alist '((link . org-clive-link)))

(defcustom org-clive-output-directory "public"
  "Directory the output files will land in."
  :group 'org-export-clive
  :type 'string)

(defcustom org-clive-after-create-page-hook nil
  "Hook that is run after creating a new weblog page."
  :group 'org-export-clive
  :type 'hook)

(defcustom org-clive-after-generate-hook ()
  "A hook run after generating the whole weblog."
  :group 'org-export-clive
  :type 'hook)

(defcustom org-clive-assets-id "assets"
  "The CUSTOM_ID of the subtree with assets."
  :group 'org-export-clive
  :type 'string)

(defun org-clive-link (link contents info)
  "Transcode a LINK object into HTML for clive."
  (let ((type (org-element-property :type link))
	(path (org-element-property :path link)))
    (if (string= type "custom-id")
	(format "<a href=\"%s\">%s</a>"
		(org-clive--pagename path)
		contents)
      (when (and (string= type "file")
		 (not (file-name-absolute-p path)))
	(org-element-put-property link :path (file-name-nondirectory path))
	(copy-file path
		   (file-name-concat org-clive-output-directory
				     (file-name-nondirectory path))
		   t t))
      (org-element-put-property link :attr_html (list ":class" "external"))
      (org-html-link link contents info))))

(defun org-clive--sluggify (string)
  "Sluggify STRING.
This means lowercasing STRING and removing everything but letters
and digits and replacing them with dashes."
  (replace-regexp-in-string
   "\\s-+"
   "-"
   (replace-regexp-in-string "[^a-z0-9 ]+" "-" (downcase string))))

(defun org-clive-create-page (title pub-date)
  "Create an Org mode heading with a page template."
  (interactive (list
		(read-string "Page title: ")
		(org-read-date nil t nil "Publication date:")))
  (org-insert-heading-after-current)
  (insert title)
  (org-set-property "PUBLICATION_DATE"
		    (org-format-timestamp
		     (org-timestamp-from-time pub-date nil t)
		     (org-time-stamp-format nil t)))
  (org-set-property "CUSTOM_ID" (org-clive--sluggify title))
  (run-hooks 'org-clive-after-create-page-hook))

(defun org-clive--pagename (string)
  "Convert STRING to a pagename.
This means appending `.html' unless there is a period in STRING."
  (if (string-search "." string)
      string
    (concat string ".html")))

(defun org-clive--filename (string)
  "Convert STRING to a filename.
This means prepending `org-clive-output-directory' and appending
`.html'."
  (file-name-concat
   org-clive-output-directory
   (org-clive--pagename string)))

(defun org-clive--get-title ()
  "Return the title of the current headline."
  (substring-no-properties (org-get-heading t t t t)))

(defun org-clive--get-custom-id ()
  "Return the `CUSTOM_ID' of the current headline.
Error out if none is present."
  (or (org-entry-get (point) "CUSTOM_ID")
      (error "Entry `%s' does not have the `CUSTOM_ID' property"
	     (org-clive--get-title))))

(defun org-clive--get-named-block-content (buffer block-name)
  "Return content of block named BLOCK-NAME as a string.
BLOCK-NAME must not be nil."
  (unless block-name
    (error "Block name not given"))
  (with-current-buffer buffer
    (let ((pos (org-babel-find-named-block block-name)))
      (if pos
	  (save-excursion
	    (goto-char pos)
	    (org-remove-indentation
	     (org-element-property :value (org-element-at-point))))
	(error "Block %s not found" block-name)))))

(defun org-clive--export-as-clive ()
  "Export the current subtree as a string."
  (let ((org-entities-user
	 (cons '("lbrace" nil nil "&lbrace;" "{" "{" "{") org-entities-user)))
    (org-export-as 'clive t nil t '(:with-toc nil :section-numbers nil))))

(defun org-clive--special-template (key all-pages source-buffer)
  "Return a special template described by string KEY.
KEY may be `pagelist' or `recent-N'.  Return nil if KEY is
neither or ALL-PAGES is nil."
  (when all-pages
    (cond ((let ((case-fold-search nil))
	     (string-match "^recent-\\([0-9]+\\)$" key))
	   (org-clive--recent (string-to-number (match-string 1 key))
			      all-pages source-buffer))
	  ((string= key "pagelist")
	   (org-clive--pagelist "pagelist" all-pages source-buffer)))))

(defun org-clive--recent (index all-pages source-buffer)
  "Return the page number INDEX from ALL-PAGES.
Use template `recent'.  This template may contain references to
`{link}', `{title}' and `{content}'."
  (let ((page (nth index all-pages)))
    (if page
	(let ((template (org-clive--get-named-block-content source-buffer "recent"))
	      (title (plist-get page :title))
	      (link (save-excursion ; TODO use (plist-get page :pagename)
		      (set-buffer source-buffer)
		      (goto-char (plist-get page :position))
		      (org-clive--pagename (org-clive--get-custom-id))))
	      (content (save-excursion
			 (set-buffer source-buffer)
			 (goto-char (plist-get page :position))
			 (org-clive--export-as-clive))))
	  ;; Replace "{recent-[0-9]+}" with an empty string to avoid infinite recursion when the
	  ;; page containing `{recent-N}' is one of the recent pages.
	  (replace-regexp-in-string
	   "{recent-[0-9]+}" ""
	   (replace-regexp-in-string
	    "{content}" content
	    (replace-regexp-in-string
	     "{title}" title
	     (replace-regexp-in-string "{link}" link template t t)
	     t t)
	    t t)))
      "")))

(defun org-clive--pagelist (template all-pages source-buffer)
  "Return the list of ALL-PAGES.
Use template TEMPLATE.  This template may contain references to
`{link}', `{title}', `{pub-date}' and `{pub-date-rfc-822}'.  It
is a very special template - it is assumed that the part between
two empty lines contains a reference to one page, and it is
repeated in the output as many times as there are pages.  For
example, to get an unordered list of pages, you may use the
following template:

<ul>

  <li>
    <a href=\"{link}\">{title}</a> ({pub-date})
  </li>

</ul>"
  (with-temp-buffer
    (insert (org-clive--get-named-block-content source-buffer template))
    (goto-char (point-min))
    (unless (search-forward "\n\n" nil t)
      (error "First line not found in the {%s} template" template))
    (let ((page-fragment (delete-and-extract-region
			  (point)
			  (if (search-forward "\n\n" nil t)
			      (point)
			    (error "Second blank line not found in the {%s} template" template)))))
      (mapc (lambda (page)
	      (let ((title (plist-get page :title))
		    (link (plist-get page :pagename))
		    (pub-date (plist-get page :pub-date))
		    (pub-date-rfc-822
		     (format-time-string "%e %b %Y %H:%M GMT"
					 (date-to-time (plist-get page :pub-date)))))
		(insert (replace-regexp-in-string
			 "{link}" link
			 (replace-regexp-in-string
			  "{title}" title
			  (replace-regexp-in-string
			   "{pub-date}" pub-date
			   (replace-regexp-in-string "{pub-date-rfc-822}"
						     pub-date-rfc-822 page-fragment
						     t t)
			   t t)
			  t t)
			 t t))))
	    all-pages)
      (buffer-string))))

(defun org-clive--generate-rss (all-pages source-buffer)
  "Return the RSS feed, generated using the `rss' template."
  (org-clive--pagelist "rss" all-pages source-buffer))

(defun org-clive--format-pub-date (pub-date)
  "Format PUB-DATE (an Org timestamp) into a human-readable form."
  (org-format-timestamp (org-timestamp-from-string pub-date) "%Y-%m-%d"))

(defun org-clive-generate-subtree (run-hooks &optional all-pages)
  "Export the current subtree as a weblog page.
Go up in Org hierarchy to find the nearest headline with the
`PUBLICATION_DATE' property.  Substitute templates marked as
`{template}'.  Built-in templates are `title', `pub-date', `year' and
`content'.  Other templates are taken from named source blocks.  To
insert literal `{', use e.g. `\lbrace'.

In interactive use, there is no way to export a subtree which
requires knowledge about all pages (like the list of all pages).
If you really need to do that, say e.g.
`\\[eval-expression] (org-clive-generate-subtree (org-clive-gather-pages \"2010-06-26\"))'."
  (interactive "P")
  (make-directory org-clive-output-directory t)
  (save-excursion
    (while (not (org-entry-get (point) "PUBLICATION_DATE"))
      (unless (org-up-heading-safe)
	(error "No entry with `PUBLICATION_DATE' found")))
    (let* ((source-buffer (current-buffer))
	   (content (org-clive--export-as-clive))
	   (title (org-clive--get-title))
	   (custom-id (org-clive--get-custom-id))
	   (pagename (org-clive--pagename custom-id))
	   (filename (org-clive--filename custom-id))
	   (pub-date (org-clive--format-pub-date (org-entry-get (point) "PUBLICATION_DATE")))
	   (year (substring pub-date 0 4))
	   (layout (org-clive--get-named-block-content source-buffer "layout"))
	   (values `(("title" . ,title)
		     ("pub-date" . ,pub-date)
		     ("year" . ,year)
		     ("content" . ,content)
		     ("id" . ,custom-id))))
      (with-temp-file filename
	(insert layout)
	(goto-char (point-min))
	(while (re-search-forward "{\\([a-z0-9_-]+\\)}" nil t)
	  (let* ((key (match-string 1))
		 ;; do not use the `default' argument of `alist-get' so
		 ;; that it is only computed when needed
		 (value (or (alist-get key values nil nil #'string=)
			    (save-match-data
			      (org-clive--special-template key all-pages source-buffer))
			    (save-match-data
			      (org-clive--get-named-block-content source-buffer key)))))
	    (replace-match value t t nil 0)
	    ;; Go back to the beginning of the replacement text, because
	    ;; it might also contain references to templates
	    (unless (string= value "")
	      (goto-char (match-beginning 0))))))
      (message "File %s generated." filename)
      ))
  (when run-hooks
    (run-hooks 'org-clive-after-generate-hook)))

(defun org-clive--get-subtree-data ()
  "Return the data about the current subtree.
Return the title, publication date, pagename and position, as
a plist."
  (let* ((title (org-clive--get-title))
	 (pub-date (org-clive--format-pub-date
		    (org-entry-get (point) "PUBLICATION_DATE")))
	 (custom-id (org-clive--get-custom-id))
	 (pagename (org-clive--pagename custom-id))
	 (position (org-entry-beginning-position)))
    (list :title title
	  :pub-date pub-date
	  :pagename pagename
	  :position position)))

(defun org-clive-gather-pages (date)
  "Gather all pages published on or before DATE.
Sort them in reverse chronological order."
  (cl-sort (org-map-entries
	    #'org-clive--get-subtree-data
	    (format "+PUBLICATION_DATE<=\"<%s>\"" date)
	    nil 'archive 'comment)
	   #'string>
	   :key (lambda (page)
		  (plist-get page :pub-date))))

(defun org-clive--generate-asset-file (element)
  "Generate an asset file from the element at point.
Call a suitable function depending on whether it's a block or
a link."
  (cl-case (org-element-type element)
    ((src-block example-block) (org-clive--generate-text-asset-file element))
    (link (org-clive--generate-binary-asset-file element))))

(defun org-clive--generate-text-asset-file (block)
  "Generate an asset file from the block at point."
  (with-temp-file (file-name-concat org-clive-output-directory
				    (org-element-property :name block))
    (insert (org-element-property :value block))
    (org-do-remove-indentation)))

(defun org-clive--generate-binary-asset-file (link)
  "Generate an asset file from the link at point."
  (let ((type (org-element-property :type link))
	(path (org-element-property :path link)))
    (when (and (string= type "file")
	       (not (file-name-absolute-p path)))
      (copy-file path
		 (file-name-concat org-clive-output-directory
				   (file-name-nondirectory path))
		 t t))))

(defun org-clive--generate-assets ()
  "Generate static (text) assets.
They are converted from named source blocks under the `Assets'
headline.  As for images, there is no need for a separate
generation step - images referenced as links are copied to
`org-clive-output-directory' when pages referencing them are
generated."
  (save-excursion
    (let ((pos (org-find-property "CUSTOM_ID" org-clive-assets-id)))
      (unless pos
	(error "Entry with CUSTOM_ID equal to \"%s\" not found" org-clive-assets-id))
      (goto-char pos)
      (let ((parsed-assets-element (save-restriction
				     (org-narrow-to-subtree)
				     (org-element-parse-buffer))))
	(org-element-map
	    parsed-assets-element
	    '(src-block link)
	  #'org-clive--generate-asset-file)))))

(defun org-clive-generate-weblog (date run-hooks)
  "Export all pages to be published on or before DATE."
  (interactive (list
		(org-read-date nil nil nil "Publication date:")
		current-prefix-arg))
  (make-directory org-clive-output-directory t)
  (save-excursion
    (org-clive--generate-assets)
    (let ((source-buffer (current-buffer))
	  (all-pages (org-clive-gather-pages date)))
      (when (org-babel-find-named-block "rss")
	(with-temp-file
	    (org-clive--filename "rss.xml")
	  (insert (org-clive--generate-rss all-pages source-buffer))))
      (mapc (lambda (page)
	      (goto-char (plist-get page :position))
	      (org-clive-generate-subtree run-hooks all-pages))
	    all-pages))
    (when run-hooks
      (run-hooks 'org-clive-after-generate-hook))))

(defun org-clive-clean-output-directory ()
  "Remove all files from `org-clive-output-directory'.
Assume there are no subdirectories there."
  (interactive)
  (mapc #'delete-file
	(directory-files
	 org-clive-output-directory t directory-files-no-dot-files-regexp)))

(provide 'org-clive)
;;; org-clive.el ends here
